# Entrevista MercadoLibre.
El precente proyecto fue programado con el propósito de complir los requisitos técnicos de la entrevista técnica de Mercado Libre. El objetivo de esta parte del proyecto (`Backend` o `Backend for Frontend`) es interactuar con la API de ML, para poder brindar la información necesaria al `Frontend`.

El lenguaje utilizado fue NodeJS/express.

# Iniciar el proyecto (local)
1. Ingresar en una colsola o terminal al root del proyecto.
1. Ejecutar el comando `yarn` ó `npm install`.
1. Ejecutar el comando `npm start`.

# Comandos útiles
1. Checkear/fixear código con lint: `yarn lint:fix`
1. Formatear código con prettier: `yarn format:fix`

# Test: TODO.

# Deploy: TODO.

# "Documentación" de servicios
| Búsqueda de productos 	| GET 	| http://localhost:4500/api/?q=search 	|
| -	| -	| -	|
| Obtener producto 	| GET 	| http://localhost:4500/api/:id 	|

# Contacto
matias.serpez@gmail.com