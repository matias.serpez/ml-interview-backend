// VENDOR
import customEnv from 'custom-env';
// SERVER
import Server from './server';

// Set enviroment variables based on envs files
customEnv.env(true);

// Define the port to use
const PORT: number = Number(process.env.APP_PORT) || 4000;

// Create a new express application instance
const server = Server.init(PORT);

server.start(() => {
  // eslint-disable-next-line no-console
  console.log(`Server is listening http://localhost:${PORT}`);
});
