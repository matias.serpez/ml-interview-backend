// VENDOR
import axios from 'axios';
// CONSTANTS
import { ML_URI } from '../constants';

export const MLService = axios.create({
  baseURL: ML_URI,
  headers: {
    'content-type': 'Application/JSON',
  },
});
