import itemsRouter from '../entities/items/routes';

export const routes = {
  itemsRouter: itemsRouter,
};
