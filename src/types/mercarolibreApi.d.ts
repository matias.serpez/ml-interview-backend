declare interface MLSearchItemsResponse {
  results: MLItem[];
}

declare interface MLItem {
  category_id: string;
  condition: string;
  currency_id: string;
  id: string;
  price: number;
  sold_quantity: string;
  shipping: {
    free_shipping: boolean;
  };
  thumbnail: string;
  title: string;
}

declare interface MLItemDescription {
  plain_text: string;
}
