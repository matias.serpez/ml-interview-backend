declare interface Author {
  name: string;
  lastname: string;
}

declare interface Price {
  currency: string;
  amount: number;
  decimals: number;
}

declare interface Author {
  name: string;
  lastname: string;
}

declare interface Item {
  id: string;
  title: string;
  price: Price;
  picture: string;
  condition: string;
  free_shipping: boolean;
  sold_quantity?: string;
  description?: string;
}

declare interface ItemsResponse {
  author: Author;
  categories: string[];
  items: Item[];
}

declare interface ItemResponse {
  author: Author;
  item: Item;
}
