// VENDOR
import express from 'express';
import https from 'https';
import cors from 'cors';
// ROUTER
import { routes } from '../routes';

export default class Server {
  public app: express.Application;

  public port: number;

  constructor(port: number) {
    this.port = port;
    this.app = express();
    this._setCommon();
    this._setRoutes();
  }

  static init(port: number): Server {
    return new Server(port);
  }

  start(callback: () => void): void {
    if (process.env.NODE_ENV !== 'local') {
      https.createServer({}, this.app).listen(this.port, callback);
    } else {
      this.app.listen(this.port, callback);
    }
  }

  _setCommon(): void {
    this.app.use(cors());
  }

  _setRoutes(): void {
    // Service staths check for kub
    this.app.get('/info', (req, res) => res.json({ status: 'OK' }));
    // Custom routes
    this.app.use('/api', routes.itemsRouter);
  }
}
