// SERVICES
import { MLService } from '../../services/mercadolibreApi';
// TRANSFORMERS
import { getSearchResult, getItemResult } from './transformers';

export async function getItems(filter: string): Promise<ItemsResponse> {
  try {
    const { data } = await MLService.get<MLSearchItemsResponse>(encodeURI(`/sites/MLA/search?q=:${filter}`));
    return getSearchResult(data);
  } catch (error) {
    throw error;
  }
}

export async function getItem(id: string): Promise<ItemResponse> {
  try {
    const { data: itemData } = await MLService.get<MLItem>(`/items/${id}`);
    const { data: descriptionData } = await MLService.get<MLItemDescription>(`/items/${id}/description`);

    return getItemResult(itemData, descriptionData);
  } catch (error) {
    throw error;
  }
}
