import _get from 'lodash/get';
import { getItemPrice } from './utils';

/**
 * Transform ML API search to Wrapper Service response.
 * @param data
 */
export function getSearchResult(data: MLSearchItemsResponse): ItemsResponse {
  const items: Item[] = [];
  const categories: string[] = [];

  data.results.forEach(result => {
    items.push({
      id: result.id,
      title: result.title,
      price: getItemPrice(result.price, result.category_id),
      picture: result.thumbnail,
      condition: result.condition,
      free_shipping: _get(result, 'shipping.free_shipping', false),
    });

    if (!categories.includes(result.category_id)) {
      categories.push(result.category_id);
    }
  });

  return {
    author: {
      name: '',
      lastname: '',
    },
    categories: categories.sort(),
    items,
  };
}

/**
 * Transform ML API item to Wrapper Service response.
 *
 * @param item
 * @param description
 */
export function getItemResult(item: MLItem, description: MLItemDescription): ItemResponse {
  return {
    author: {
      name: '',
      lastname: '',
    },
    item: {
      id: item.id,
      title: item.title,
      price: getItemPrice(item.price, item.category_id),
      picture: item.thumbnail,
      condition: item.condition,
      free_shipping: _get(item, 'shipping.free_shipping', false),
      sold_quantity: item.sold_quantity,
      description: description.plain_text,
    },
  };
}
