import _get from 'lodash/get';
import { Router, Request, Response } from 'express';
import { getItems, getItem } from './controller';
import { NextFunction } from 'connect';

const itemsRouter = Router();

itemsRouter.get('/', async (req: Request, res: Response, next: NextFunction) => {
  const searchQuery = _get(req, 'query.q', '');
  try {
    const result = await getItems(searchQuery);
    res.json(result).status(200);
  } catch (error) {
    next(error);
  }
});

itemsRouter.get('/:itemId', async (req: Request, res: Response, next: NextFunction) => {
  const id = _get(req, 'params.itemId');

  try {
    const result = await getItem(id);
    res.json(result).status(200);
  } catch (error) {
    next(error);
  }
});

export default itemsRouter;
