export function getItemPrice(price: number, currency_id: string): Price {
  const floatPrice = parseFloat(String(price || 0)).toFixed(2);
  const splitedPrice = floatPrice.split('.');

  return {
    currency: currency_id,
    amount: Number(splitedPrice[0]),
    decimals: Number(splitedPrice[1]),
  };
}
